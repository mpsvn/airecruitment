from setuptools import setup

setup(name='cv_parsing',
      version='1.0',
      # list folders, not files
      packages=['cv_parsing',
                'cv_parsing.test'],
      # scripts=['capitalize/bin/cap_script.py'],
      package_data={'': ['data/*.txt']},
      )

