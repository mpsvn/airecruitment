from cv_parsing.core.convert_to_text import convert_to_text
import cv_parsing.core.model as model


def text_classify(file):
    result_json = {
        'CanId': '',
        'CanEmail': '',
        'CanTelNum': '',
        'CanFullName': '',
        'CanDob': '',
        'SexCode': '',
        'CanAddress': '',
        'CanNationality': '',
        'CanFileCv': '',
        'Interest': '',
        'Strength': '',
        'FutureGoals': '',
        'DesiredJob': '',
        'ExperienceYears': '',
        'SoftSkill': '',
        'Skill': '',
        'LearningDiploma': '',
        'Specialize': '',
        'IsUpdate': '',
        'LearningAwards': '',
        'WorkingAwards': '',
        'Diploma': '',
        'CanImgCand': '',
        'LanguageCertificate': '',
        'ReferencePerson': '',
        'ReferencePersonPosition': '',
        'Project': '',
        'ProjectPosition': '',
        'CompanyPosition': '',
        'CompanyWorked': '',
        'UpdateDate': '',
        'JobTitle': '',
        'Point': ''
    }
    cv = convert_to_text(file)
    for text_object in cv:
        text = text_object[0][0:-1].replace("\n", " ")
        res = model.predict(text)
        if len(result_json[res]) > 0:
            result_json[res] += "; " + text
        else:
            result_json[res] += text
    return result_json



