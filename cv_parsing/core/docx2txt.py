from docx2pdf import convert
from cv_parsing.core.pdf2txt import parsing_pdf_cv


def parsing_docx_cv(file):
    pdf_file = file[0:-4] + "pdf"
    convert(file, pdf_file)
    return parsing_pdf_cv(pdf_file)



