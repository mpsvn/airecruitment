from pdfminer.pdfparser import PDFParser
from pdfminer.pdfdocument import PDFDocument
from pdfminer.pdfpage import PDFPage
from pdfminer.pdfpage import PDFTextExtractionNotAllowed
from pdfminer.pdfinterp import PDFResourceManager
from pdfminer.pdfinterp import PDFPageInterpreter
from pdfminer.layout import LAParams
from pdfminer.converter import PDFPageAggregator
import pdfminer
import pandas as pd


def parse_obj(lt_objs):
    bbox_row = []
    bbox_column = []
    text = []
    # loop over the object list
    for obj in lt_objs:

        # if it's a textbox, print text and location
        if isinstance(obj, pdfminer.layout.LTTextBoxHorizontal):
            # print("%s, %s" % (obj.bbox, obj.get_text()))
            bbox_row.append(obj.bbox[1])
            bbox_column.append(obj.bbox[0])
            text.append(obj.get_text())
        # if it's a container, recurse
        elif isinstance(obj, pdfminer.layout.LTFigure):
            parse_obj(obj._objs)
    return bbox_column, bbox_row, text


def line_grouping(bbox_row):
    res = pd.Series(range(len(bbox_row))).groupby(bbox_row).apply(list).tolist()
    return res


def parsing_pdf_cv(file):
    fp = open(file, 'rb')
    # Create a PDF parser object associated with the file object.
    parser = PDFParser(fp)

    # Create a PDF document object that stores the document structure.
    # Password for initialization as 2nd parameter
    document = PDFDocument(parser)

    # Check if the document allows text extraction. If not, abort.
    if not document.is_extractable:
        raise PDFTextExtractionNotAllowed

    # Create a PDF resource manager object that stores shared resources.
    rsrcmgr = PDFResourceManager()

    # BEGIN LAYOUT ANALYSIS
    # Set parameters for analysis.
    laparams = LAParams(line_margin=0.2)

    # Create a PDF page aggregator object.
    device = PDFPageAggregator(rsrcmgr, laparams=laparams)

    # Create a PDF interpreter object.
    interpreter = PDFPageInterpreter(rsrcmgr, device)
    result_full_text = []
    for page in PDFPage.create_pages(document):
        # read the page into a layout object
        interpreter.process_page(page)
        layout = device.get_result()

        # extract text from this object
        bbox_column, bbox_row, text = parse_obj(layout._objs)
        cord = line_grouping(bbox_row)
        for i in range(len(cord)):
            row = bbox_row[cord[-i - 1][0]]
            column = 0
            txt = ''
            box_word = []
            if len(cord[-i - 1]) > 1:
                for j in range(len(cord[-i - 1])):
                    box_word.append([bbox_column[cord[-i - 1][j]], cord[-i - 1][j]])
                box_word = sorted(box_word, key=lambda x: x[0])
                column = box_word[0][0]
                for j in range(len(box_word)):
                    if j < len(box_word) - 1:
                        res = text[box_word[j][1]].replace("\n", "_")
                    else:
                        res = text[box_word[j][1]]
                    txt = txt + res
            else:
                txt = text[cord[-i - 1][0]]
                column = bbox_column[cord[-i - 1][0]]
            if len(txt.strip()) > 0:
                result_full_text.append([txt, row, column])
    return result_full_text


