from cv_parsing.core.pdf2txt import parsing_pdf_cv
from cv_parsing.core.docx2txt import parsing_docx_cv


def convert_to_text(file):
    if file.find(".pdf") != -1:
        return parsing_pdf_cv(file)
    elif file.find(".docx") != -1:
        return parsing_docx_cv(file)
    return 0


