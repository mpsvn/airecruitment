# %%
from vncorenlp import VnCoreNLP
from cv_parsing import utils
import cv_parsing.data.ref_model.vncorenlp as vnnlp_path
from cv_parsing.data import clf_model
import dill
import numpy as np
import time

from cv_parsing.utils import preprocess_string
import torch
from transformers import AutoModel, AutoTokenizer

# %%
annotator = VnCoreNLP(utils.get_pkg_file(vnnlp_path, 'VnCoreNLP-1.1.1.jar'),
                      annotators="wseg",
                      max_heap_size='-Xmx500m')
phobert = AutoModel.from_pretrained("vinai/phobert-base")
# For transformers v4.x+: 
tokenizer = AutoTokenizer.from_pretrained("vinai/phobert-base", use_fast=False)

# %%
with open(utils.get_pkg_file(clf_model, 'clf-model.dill'), 'rb') as fr:
    clf = dill.load(fr)


# %%
def make_bert_features(text, max_len=100, max_split=70):
    """
    text: paragraph

    Returns:

    """
    v_tokenized = []
    # max_len = 100 # Mỗi câu dài tối đa 100 từ
    lines = text.split('\n')  # split text into lines

    word_lines = list(map(annotator.tokenize, lines))

    for wd_line in word_lines:
        for wd_sent in wd_line:
            if len(wd_sent) < max_split:
                line = " ".join(wd_sent)
                # print("Word segment  = ", line)
                # Tokenize bởi BERT
                line = tokenizer.encode(line)
                v_tokenized.append(line)

            else:
                split_sents = []
                for i in range(0, len(wd_sent), max_split):
                    split_sents.append(wd_sent[i: min(i + max_split, len(wd_sent) - 1)])
                for sp_sent in split_sents:
                    line = " ".join(sp_sent)
                    line = tokenizer.encode(line)
                    v_tokenized.append(line)

    # Chèn thêm số 1 vào cuối câu nếu như không đủ 100 từ
    # max_token_len = max(list( map( len, v_tokenized)))
    # if max_token_len > max_len:
    #     print(text)

    # np.pad is slower than python loop
    # print('Time inspect')
    # start = time.time() 
    # padded = np.array([ np.pad(i[:max_len], (0, max(0, max_len-len(i))), constant_values=1) for i in v_tokenized])
    # attention_mask = np.where(padded == 1, 0., 1.)
    # padded = torch.tensor(padded, dtype=torch.long)
    # print("numpy.pad: ", time.time()-start)

    # start = time.time()
    padded = np.array([i[:max_len] + [1] * (max_len - len(i)) for i in v_tokenized])
    # padded = padded.astype(np.int64)
    # Đánh dấu các từ thêm vào = 0 để không tính vào quá trình lấy features

    attention_mask = np.where(padded == 1, 0., 1.)
    padded = torch.tensor(padded, dtype=torch.long)
    # print("Pythonic: ", time.time() - start)

    attention_mask = torch.tensor(attention_mask)

    # Lấy features dầu ra từ BERT
    with torch.no_grad():
        last_hidden_states = phobert(input_ids=padded, attention_mask=attention_mask)

    v_features = last_hidden_states[0][:, 0, :].tolist()
    return v_features


def predict(input_text: str) -> str:
    assert type(input_text) is str
    input_text = preprocess_string(input_text)
    text_feature = make_bert_features(input_text)
    res = clf.predict(text_feature)[0]
    return res
# %%
