import logging

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)
import timeit

start_time = timeit.default_timer()
logger.info(f"{__name__} starts loading ...")
from . import core 
from . import utils
logger.info(f"{__name__} loaded after {timeit.default_timer() - start_time}")
# from .utils import preprocess_string

fetch = core.predict
