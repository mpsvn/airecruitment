# %%
from gensim.parsing import preprocessing
import unicodedata
import re

__all__ = [
        'preprocess_string'
        ]
R_VALID = re.compile(r'[^aAàÀảẢãÃáÁạẠăĂằẰẳẲẵẴắẮặẶâÂầẦẩẨẫẪấẤậẬbBcCdDđĐeEèÈẻẺẽẼéÉẹẸêÊềỀểỂễỄếẾệỆfFgGhHiIìÌỉỈĩĨíÍịỊjJkKlLmMnNoOòÒỏỎõÕóÓọỌôÔồỒổỔỗỖốỐộỘơƠờỜởỞỡỠớỚợỢpPqQrRsStTuUùÙủỦũŨúÚụỤưƯừỪửỬữỮứỨựỰvVwWxXyYỳỲỷỶỹỸýÝỵỴzZ1234567890!@#$%^&*()_+-=[];,.{}:"\'<>?]')
R_WHITESPACE = re.compile(r"( )+")
R_VALID_PUNCT = re.compile(r"(?<=(\w|\s))\.(?=(\s))")
R_EMPTY_LINES = re.compile(r"(\n)+")

# %%
# def remove_stopwords(s, repl=' '):
#     for w in STOP_WORDS:
#         s.replace(w, ' ')
#     return s

def remove_multiple_whitespace(s: str):
    pass

punct = r"[!#'()*+,\-:;<=>?[\]^_`{|}~\"]" # not included "."
PREPROC_PIPELINE = [
    # to_unicode, # unicode
    lambda s: unicodedata.normalize('NFC', s), # unicode
    preprocessing.strip_tags, # remove html tag
    # lambda s: re.sub(r"(?<=.)\.(?=\s+)", r"\n", s), # separate sentences into lines
    lambda s: re.sub(r"(?<=\w)\.(?=\n)", r".", s),
    # lambda s: re.sub(r"\n", r". ", s),
    lambda s: re.sub(r"(\n{2,})", r"\n", s), # remove multiple line-breaks
    # lambda s: re.sub(r"(?<=\w[^\.])\n(?=(\w+|[ ]+))", r'. ', s),

    # preprocessing.strip_punctuation, # remove all punctuation
    lambda s: re.sub(punct, ' ', s),
    lambda s: re.sub(r"\.{2,}", ' ', s),

    lambda s: re.sub(r"\s+\.\s+", r". ",s),  # for "." sentence


    lambda s: R_VALID.sub(r' ', s), # remove useless chars
    lambda s: R_WHITESPACE.sub(r' ', s), # multiple whitespace

    lambda s: re.sub( r"\s+(?=\n)", "", s), # remove white spaces at the end of line

    lambda s: re.sub( r"(?<=\n)\s+", "", s), # remove white spaces at the begin of line
    str.strip, # first/last spaces
    # remove_stopwords,
]

def preprocess_string(s: str, filters=PREPROC_PIPELINE):
    for f in filters:
        s = f(s)
    return s
