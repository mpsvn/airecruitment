from .preprocessing import preprocess_string
from typing import Dict, List
__all__ = [

        ]
import importlib
import importlib.resources as resources
# try:
#     import importlib.resources as resources
# except ImportError:
#     # Try backported to PY<37 `importlib_resources`.
#     import importlib_resources as resources

def get_pkg_file (pkg, fname) -> str: 
    """
    Get absolute path of package's data file

    

    Parameters
    ----------
    pkg : package
        [TODO:description]
    fname : name of file``
        [TODO:description]

    Returns
    -------
    str:
        absolute file path to package's data file
    """
    assert type(fname) is str
    with resources.path(pkg, fname) as p:
        res_path = p
    return str(res_path)
