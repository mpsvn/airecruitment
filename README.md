# AI for recruitment

## Features

- Read documents in pdf, docx to structured text object.
- Classify text object into CV's fields.
```
import cv_parsing
cv_parsing.fetch("tranducnam@hust.edu.vn")
>> 'CanEmail'
```
- Rank CV by scoring extracted fields.

## Installation

### Docker 

```bash
docker pull huypl53/ai_recruitment:v1.0
```



### Manually installation

#### Reference models

```bash
cd airecruitment/cv_parsing/data/ref_model
mkdir -p vncorenlp/models/wordsegmenter
wget https://raw.githubusercontent.com/vncorenlp/VnCoreNLP/master/VnCoreNLP-1.1.1.jar
wget https://raw.githubusercontent.com/vncorenlp/VnCoreNLP/master/models/wordsegmenter/vi-vocab
wget https://raw.githubusercontent.com/vncorenlp/VnCoreNLP/master/models/wordsegmenter/wordsegmenter.rdr
mv VnCoreNLP-1.1.1.jar vncorenlp/ 
mv vi-vocab vncorenlp/models/wordsegmenter/
mv wordsegmenter.rdr vncorenlp/models/wordsegmenter/
```

### Implementing
