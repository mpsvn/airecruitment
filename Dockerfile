FROM huypl53/ai_recruitment:v1.0

RUN apt update
RUN conda update conda
RUN conda activate nlp

RUN mkdir environment 

WORKDIR /environment
ADD . /environment
WORKDIR airecruitment

CMD ["conda", "env", "update", "--file", "requirement.yml"]

# 172.17.0.1 for docker container, should check again by "docker inspect"
CMD ["python", "server.py", "172.17.0.2", "80" ]
