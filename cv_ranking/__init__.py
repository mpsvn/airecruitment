import logging
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)
import timeit

start_time = timeit.default_timer()
from . import core
logger.info(f"{__name__} starts loading ...")
logger.info(f"{__name__} loaded after {timeit.default_timer() - start_time}")
