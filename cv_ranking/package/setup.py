from setuptools import setup

setup(name='cv_ranking',
      version='1.0',
      # list folders, not files
      packages=['cv_ranking',
                'cv_ranking.test'],
      # scripts=['capitalize/bin/cap_script.py'],
      package_data={'': ['data/*.txt']},
      )

