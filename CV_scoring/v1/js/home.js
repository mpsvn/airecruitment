function readURL(input) {
    if (input.files && input.files[0]) {
        $('.image-upload-wrap').hide();
        $('.file-upload-image').html(input.files[0].name);
        $('.file-upload-content').show();
        $('.image-title').html(input.files[0].name);
        $('#result-text').html('None');
    } else {
        removeUpload();
    }
}

function sendData() {
    /*4 means request is complete , if you don't call `Request.send(data)` like I have in the previous function, in the line above ,then it means you have not even started the request (which is 1), if 1 is not even reached in the ready state, how can you check for 4 or for status 200. You must send first , so that ready state changes and sendData is called*/
    if (Request.readyState == 4 && Request.status == 200) {
        alert('Data sent ...');
    } else {
        alert('Connection FAIL,\nCheck connection and Retry !!!');
        console.log(Request);
    }
}

function uploadFile() {
    // console.log('Inside load');
    formData = new FormData();
    formData.append('the_file', $('#input-file')[0].files[0]);
    URL = "http://172.17.0.1:4096/";
    $.ajax({
        type: "POST",
        url: URL,
        data: formData,
        processData: false,
        contentType: false,
        dataType: 'json',
        success: function(msg, status, jqXHR){
          console.log(msg);
          $('.result-text').html(msg);
          // alert('ajax responsed');
        }
    });
    /* var xhr = new XMLHttpRequest();
    xhr.open( "POST", URL, true );
    xhr.setRequestHeader('Content-Type','multipart/form-data; boundary=' + 'blob');
    xhr.setRequestHeader('Access-Control-Allow-Origin', '*');
    xhr.send(formData); */
}

function removeUpload() {
    $('.file-upload-input').replaceWith($('.file-upload-input').clone());
    $('.file-upload-content').hide();
    $('.image-upload-wrap').show();
}

function reloadHome() {
    location.reload("home.html")
}
