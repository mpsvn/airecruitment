from setuptools import setup

setup(name='airecruitment',
      version='1.0',
      # list folders, not files
      packages=['cv_ranking',
                'cv_parsing',
                # 'cv_parsing.data.model_ref'
                ],
      # scripts=['capitalize/bin/cap_script.py'],
      package_data={'': ['*/data/*']},
      )

