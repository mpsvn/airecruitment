# %%
from flask import Flask
from flask import request
from flask_cors import CORS
from werkzeug.utils import secure_filename
import cv_parsing
import cv_ranking
import json
# WSGI app
import sys

# https://flask.palletsprojects.com/en/1.1.x/api/#flask.Flask
app = Flask(__name__) # arg1 - name of the app

CORS(app)
@app.route('/', methods=['GET', 'POST'])
def upload_file():
    if request.method == 'POST':
        print(request.files)
        f = request.files['the_file']
        f.save(secure_filename(f.filename))
        json_data = cv_parsing.core.text_classify(f.filename)
        result = cv_ranking.core.score(json_data)
        return json.dumps(result), 200
        # return json.dumps([{'foo': 'bar'}, {'foo2': 'bar2'}]), 200
    else:
        return 'Unsuccessfully', 404

# for dev app only
if __name__ == '__main__':
    # docker host interface
    # app.run(host="172.17.0.1", port=4096, debug=True, )
    address = sys.argv[1]
    port = int(sys.argv[2])
    app.run(host=address, port=port, debug=True )
    # app.run(host="127.0.0.1", port=4096, debug=True )
